var app = new Vue({
    el: "#app",
    data: {
        cartes: {
            "lille": ["citadelle", "république", "furet"],
            "coloc": ["déguisement", "nagui", "caddie"],
            "savoie": ["ESF", "penchée", "raquettes"],
            "autre": ["papayoo", "paddle", "cœur", "puce", "selfie", "Garfield", "slack"],
        },
        cartes1: {
            "la run" : ["éruption", "takamaka", "bonite", "dodo"],
            "islande":  ["silice", "cheval", "eyjafjallajökull"],
            "paris":   ["montmartre", "j'ai rando"],
            "autre":   ["Faguo", "Photographe", "Federer", "Orthophonie", "Danse", "Gobelins", "Coldplay"]
        },
        cartes2: {
            "evjf": ["catamaran", "lama", "ramadan"],
            "evg": ["arbitre", "TTMC", "1 million d'€"],
            "Caz": ["frigo", "cluedo", "manoir"],
            "autre": ["Jim Carrey", "Mototaxi", "Marsanne", "Suisse", "Cambodge", "millet", "Harun Tazieff"]
        },
        cartes3: {
            "zouzs": ["éternel", "amitié", "sirène"],
            "autre": ["endocrinien", "pertuisane", "tripophobie", "munificence", "récipiendaire", "salmigondis", "marmoréen", "fuligineux", "parturition", "zeugma", "sérendipité", "schefflera", "liquidambar"]
        },
        resultats: {
            "lille": "lille.png",
            "coloc": "coloc.jpeg",
            "savoie": "savoie.jpeg",
            "la run": "_MG_7566.jpg",
            "islande": "_MG_9585.jpg",
            "paris": "",
            "evjf": "evjf.jpeg",
            "evg": "evg.jpeg",
            "Caz": "caz.jpeg",
            "zouzs": "zouz.jpeg"
        },
        anecdote: "",
        partie: {},
        indice : "",
        indiceCourant: 0,
        listeIndices: [],
        modalVisible: false,
        indicePartie: 0
    },
    created: function () {
        this.initialiser();
    },
    methods: {
        initialiser: function () {
            this.listeIndices = _.pull(_.keys(this.cartes), 'autre');
            this.indiceCourant = 0;
            this.indice = this.listeIndices[this.indiceCourant];
            this.partie = {};
        },
        selectionneCarte: function (mot) {
            if(this.partie[mot]) { return; }
            var reponse = this.cartes[this.indice].includes(mot);
            this.$set(this.partie, mot, reponse);
            this.verifierVictoire();
        },
        verifierVictoire: function () {
            var listeCartesValide = this.cartes[this.indice];
            var conditionDeVictoire = _.pickBy(this.partie, function (v, k) {
                return _.includes(listeCartesValide, k) && v === true;
            });
            if (_.keys(conditionDeVictoire).length === listeCartesValide.length) {
                this.victoirePartie();
            }
        },
        victoirePartie: function () {
            this.anecdote = this.resultats[this.indice];
            this.modalVisible = true;
        },
        autreIndice: function () {
            this.indiceCourant++;
            if (this.indiceCourant >= this.listeIndices.length) {
                alert('Fin de cette partie mon ami !');
                this.nouvellePartie();
                return;
            }
            this.indice = this.listeIndices[this.indiceCourant];
            this.partie = _.pickBy(this.partie, function (v, k) {
                return v === true;
            });
        },
        nouvellePartie: function () {
            if (this.indicePartie >= 3) {
                alert ("C'est la fin de jeu <3 !");
                return;
            }
            this.indicePartie++;
            if (this.indicePartie === 1)
                this.cartes = this.cartes1;
            if (this.indicePartie === 2)
                this.cartes = this.cartes2;
            if (this.indicePartie === 3)
                this.cartes = this.cartes3;
            this.initialiser();
        }
    },
    computed: {
        getChunkCartes : function () {
            return _.chunk(_.shuffle(_.flatten(_.values(this.cartes))), 4);
        }
    }
})
